using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using Methods_Detector;

namespace Methods_Detector
{
    public class Blob_Detect
    {
        public Rectangle matches5;
        public Image<Bgr,byte> blob;
        public Image<Gray, byte> bin01_;//imagen que se exporta para la muestra de los blob 
        public int _coincid = 0;
        public System.Drawing.PointF matches3;
        Methods_Detector.Binarizacion _bin01 = new Methods_Detector.Binarizacion();
        double countblack;
        double countwhite;
        public Contour<Point> contours;
        
        Image<Gray, Byte> candidate;
        /*
        //desde aqui son los 5 matches usados
        public int matches3;

        public System.Drawing.Size matches4;

        

        public int matchesw_;
 
        public int matchesh;
        */
        public static Image<Gray, byte> GetRedPixelMask(Image<Ycc, byte> image)
        {
            Image<Gray, byte> result;
            using (Image<Hsv, byte> hsv = image.Convert<Hsv, byte>())
            {
                Image<Gray, byte>[] channels = hsv.Split();
                try
                {
                    CvInvoke.cvInRangeS(channels[0], new MCvScalar(20.0), new MCvScalar(210.0), channels[0]);
                    channels[0]._Not();
                    channels[1]._ThresholdBinary(new Gray(11.0), new Gray(230.0));
                    CvInvoke.cvAnd(channels[0], channels[1], channels[0], System.IntPtr.Zero);
                }
                finally
                {
                    channels[1].Dispose();
                    channels[2].Dispose();
                }
                result = channels[0];
            }
            return result; //resultado de filtrar una imagen espacio de estados HSV para obtener el color rojo de los pixeles 
        }

        public void DetectBox(Image<Ycc, byte> img,List<Image<Gray, 
                             byte>> BoxList, List<System.Drawing.Rectangle> boxList,
                             out Rectangle match6, out Image<Bgr, byte> blobimg, out Image<Gray, byte> Cannyb, 
                             out Image<Gray, byte> Smoothed, out int coincid, out System.Drawing.PointF match2, out double countblack1, out double countwhite1 )
        { // out int match2, out System.Drawing.Size match5, out int match_h, out int matchw, 
            coincid = 0;
            Image<Ycc, byte> smoothImg = img.SmoothGaussian(5, 5, 1.5, 1.5);
            
            //Image<Gray, byte> smoothedRedMask = Blob_Detect.GetRedPixelMask(smoothImg);
            Image<Gray, byte> smoothedRedMask = smoothImg.Convert<Gray,byte>();
           // smoothedRedMask._Dilate(1);
            //smoothedRedMask._Erode(1);
            _bin01.bin(smoothedRedMask, out bin01_, out countblack, out countwhite);
            countblack1 = countblack;
            countwhite1 = countwhite;
            Image<Gray, byte> cannyb = bin01_.Canny(100.0, 50.0);

           
            using (Image<Gray, byte> canny = bin01_.Canny(100.0, 50.0))
            //using (smoothedRedMask)
            {        
                using (new MemStorage())
                {
                    CvBlobs resultingWebcamBlobs = new CvBlobs();
                    CvBlobDetector bDetect = new CvBlobDetector();
                    //uint numWebcamBlobsFound = bDetect.Detect(smoothedRedMask, resultingWebcamBlobs);
                    uint numWebcamBlobsFound = bDetect.Detect(canny, resultingWebcamBlobs);
                    //Image<Bgr, byte> blobImg = bDetect.DrawBlobs(smoothedRedMask, resultingWebcamBlobs, CvBlobDetector.BlobRenderType.Default, 0.5);
                    Image<Bgr, byte> blobImg = bDetect.DrawBlobs(canny, resultingWebcamBlobs, CvBlobDetector.BlobRenderType.Default, 0.5);
                    foreach (CvBlob targetBlob in resultingWebcamBlobs.Values)
                    {/*
                        if (targetBlob.BoundingBox.X != 0 && targetBlob.BoundingBox.Width > 150 && 
                            targetBlob.BoundingBox.Y != 0 && targetBlob.BoundingBox.Width < 300 && 
                            targetBlob.BoundingBox.Width != 210 && targetBlob.BoundingBox.Height != 65 &&
                            targetBlob.BoundingBox.Width != 670 && targetBlob.BoundingBox.Height != 164 &&
                            targetBlob.BoundingBox.Width != 275 && targetBlob.BoundingBox.Height != 66 &&
                            targetBlob.BoundingBox.Width != 164 && targetBlob.BoundingBox.Height != 34 &&
                            targetBlob.BoundingBox.Width != 196 && targetBlob.BoundingBox.Height != 50 &&
                            targetBlob.BoundingBox.Width != 162 && targetBlob.BoundingBox.Height != 34 &&
                            targetBlob.BoundingBox.Width != 180 && targetBlob.BoundingBox.Height != 50 &&
                            targetBlob.BoundingBox.Width != 176 && targetBlob.BoundingBox.Height != 209 &&
                            targetBlob.BoundingBox.Width != 290 && targetBlob.BoundingBox.Height != 173 &&
                            targetBlob.BoundingBox.Width != 180 && targetBlob.BoundingBox.Height != 50 &&
                            targetBlob.BoundingBox.Width != 221 && targetBlob.BoundingBox.Height != 84 &&
                            targetBlob.BoundingBox.Width != 161 && targetBlob.BoundingBox.Height != 44 &&
                            targetBlob.BoundingBox.Width != 217 && targetBlob.BoundingBox.Height != 44 &&
                            targetBlob.BoundingBox.Width != 239 && targetBlob.BoundingBox.Height != 61 &&
                            targetBlob.BoundingBox.Width != 167 && targetBlob.BoundingBox.Height != 127 &&
                            targetBlob.BoundingBox.Width != 175 && targetBlob.BoundingBox.Height != 139 &&
                            targetBlob.BoundingBox.Width != 161 && targetBlob.BoundingBox.Height != 42 &&
                            targetBlob.BoundingBox.Width != 199 && targetBlob.BoundingBox.Height != 138 &&
                            targetBlob.BoundingBox.Width != 175 && targetBlob.BoundingBox.Height != 283 &&
                            targetBlob.BoundingBox.Width != 191 && targetBlob.BoundingBox.Height != 239 &&
                            targetBlob.BoundingBox.Width != 175 && targetBlob.BoundingBox.Height != 283 &&
                            targetBlob.BoundingBox.Width != 258 && targetBlob.BoundingBox.Height != 296 &&
                            targetBlob.BoundingBox.Width != 258 && targetBlob.BoundingBox.Height != 281 &&
                            targetBlob.BoundingBox.Width != 192 && targetBlob.BoundingBox.Height != 136 &&
                            targetBlob.BoundingBox.Width != 195 && targetBlob.BoundingBox.Height != 35 &&
                            targetBlob.BoundingBox.Width != 229 && targetBlob.BoundingBox.Height != 252 &&
                            targetBlob.BoundingBox.Width != 186 && targetBlob.BoundingBox.Height != 64 &&
                            targetBlob.BoundingBox.Width != 208 && targetBlob.BoundingBox.Height != 80 &&
                            targetBlob.BoundingBox.Width != 156 && targetBlob.BoundingBox.Height != 77 &&
                            targetBlob.BoundingBox.Width != 224 && targetBlob.BoundingBox.Height != 241 &&
                            targetBlob.BoundingBox.Width != 207 && targetBlob.BoundingBox.Height != 108 &&
                            targetBlob.BoundingBox.Width != 207 && targetBlob.BoundingBox.Height != 108 &&
                            targetBlob.BoundingBox.Width != 160 && targetBlob.BoundingBox.Height != 56 &&
                            targetBlob.BoundingBox.Width != 207 && targetBlob.BoundingBox.Height != 108 &&
                            targetBlob.BoundingBox.Width != 231 && targetBlob.BoundingBox.Height != 100 &&
                            targetBlob.BoundingBox.Width != 227 && targetBlob.BoundingBox.Height != 98 &&
                            targetBlob.BoundingBox.Width != 289 && targetBlob.BoundingBox.Height != 133 &&
                            targetBlob.BoundingBox.Width != 178 && targetBlob.BoundingBox.Height != 130 &&
                            targetBlob.BoundingBox.Width != 151 && targetBlob.BoundingBox.Height != 117 &&
                            targetBlob.BoundingBox.Width != 152 && targetBlob.BoundingBox.Height != 117 &&
                            targetBlob.BoundingBox.Width != 153 && targetBlob.BoundingBox.Height != 117 &&
                            targetBlob.BoundingBox.Width != 165 && targetBlob.BoundingBox.Height != 111 &&
                            targetBlob.BoundingBox.Width != 163 && targetBlob.BoundingBox.Height != 110 &&
                            targetBlob.BoundingBox.Width != 263 && targetBlob.BoundingBox.Height != 67 &&
                            targetBlob.BoundingBox.Width != 267 && targetBlob.BoundingBox.Height != 69 &&
                            targetBlob.BoundingBox.Width != 268 && targetBlob.BoundingBox.Height != 68 &&
                            targetBlob.BoundingBox.Width != 193 && targetBlob.BoundingBox.Height != 126 &&
                            targetBlob.BoundingBox.Width != 173 && targetBlob.BoundingBox.Height != 27 &&
                            targetBlob.BoundingBox.Width != 173 && targetBlob.BoundingBox.Height != 26 &&
                            targetBlob.BoundingBox.Width != 225 && targetBlob.BoundingBox.Height != 128 &&
                            targetBlob.BoundingBox.Width != 223 && targetBlob.BoundingBox.Height != 146 &&
                            targetBlob.BoundingBox.Width != 159 && targetBlob.BoundingBox.Height != 97 &&
                            targetBlob.BoundingBox.Width != 158 && targetBlob.BoundingBox.Height != 48 &&
                            targetBlob.BoundingBox.Width != 289 && targetBlob.BoundingBox.Height != 133 &&
                            targetBlob.BoundingBox.Width != 211 && targetBlob.BoundingBox.Height != 78 &&
                            targetBlob.BoundingBox.Width != 173 && targetBlob.BoundingBox.Height != 49 &&
                            targetBlob.BoundingBox.Width != 173 && targetBlob.BoundingBox.Height != 63 &&
                            targetBlob.BoundingBox.Width != 173 && targetBlob.BoundingBox.Height != 55 &&
                            targetBlob.BoundingBox.Width != 193 && targetBlob.BoundingBox.Height != 63 &&
                            targetBlob.BoundingBox.Width != 189 && targetBlob.BoundingBox.Height != 230 &&
                            targetBlob.BoundingBox.Width != 189 && targetBlob.BoundingBox.Height != 232 &&
                            targetBlob.BoundingBox.Width != 189 && targetBlob.BoundingBox.Height != 224 &&
                            targetBlob.BoundingBox.Width != 189 && targetBlob.BoundingBox.Height != 236 &&
                            targetBlob.BoundingBox.Width != 189 && targetBlob.BoundingBox.Height != 279 &&
                            targetBlob.BoundingBox.Width != 189 && targetBlob.BoundingBox.Height != 60 &&
                            targetBlob.BoundingBox.Width != 189 && targetBlob.BoundingBox.Height != 278 &&
                            targetBlob.BoundingBox.Width != 230 && targetBlob.BoundingBox.Height != 60 &&
                            targetBlob.BoundingBox.Width != 240 && targetBlob.BoundingBox.Height != 60 &&
                            targetBlob.BoundingBox.Width != 189 && targetBlob.BoundingBox.Height != 278 &&
                            targetBlob.BoundingBox.Width != 172 && targetBlob.BoundingBox.Height != 161 &&
                            targetBlob.BoundingBox.Width != 256 && targetBlob.BoundingBox.Height != 301 &&
                            targetBlob.BoundingBox.Width != 166 && targetBlob.BoundingBox.Height != 83 &&
                            targetBlob.BoundingBox.Width != 233 && targetBlob.BoundingBox.Height != 131 &&
                            targetBlob.BoundingBox.Width != 296 && targetBlob.BoundingBox.Height != 125 &&
                            targetBlob.BoundingBox.Width != 209 && targetBlob.BoundingBox.Height != 124 &&
                            targetBlob.BoundingBox.Width != 226 && targetBlob.BoundingBox.Height != 132 &&
                            targetBlob.BoundingBox.Width != 216 && targetBlob.BoundingBox.Height != 125 &&
                            targetBlob.BoundingBox.Width != 177 && targetBlob.BoundingBox.Height != 132 &&
                            targetBlob.BoundingBox.Width != 188 && targetBlob.BoundingBox.Height != 300 &&
                            targetBlob.BoundingBox.Width != 188 && targetBlob.BoundingBox.Height != 299 &&
                            targetBlob.BoundingBox.Width != 253 && targetBlob.BoundingBox.Height != 140 &&
                            targetBlob.BoundingBox.Width != 253 && targetBlob.BoundingBox.Height != 142 &&
                            targetBlob.BoundingBox.Width != 298 && targetBlob.BoundingBox.Height != 38 &&
                            targetBlob.BoundingBox.Width != 274 && targetBlob.BoundingBox.Height != 40 &&
                            targetBlob.BoundingBox.Width != 266 && targetBlob.BoundingBox.Height != 40 &&
                            targetBlob.BoundingBox.Width != 170 && targetBlob.BoundingBox.Height != 141 && 
                            targetBlob.BoundingBox.Width != 279 && targetBlob.BoundingBox.Height != 145 &&
                            targetBlob.BoundingBox.Width != 279 && targetBlob.BoundingBox.Height != 147 &&
                            targetBlob.BoundingBox.Width != 287 && targetBlob.BoundingBox.Height != 153 &&
                            targetBlob.BoundingBox.Width != 281 && targetBlob.BoundingBox.Height != 143 &&
                            targetBlob.BoundingBox.Width != 154 && targetBlob.BoundingBox.Height != 75 &&
                            targetBlob.BoundingBox.Width != 154 && targetBlob.BoundingBox.Height != 71 &&
                            targetBlob.BoundingBox.Width != 157 && targetBlob.BoundingBox.Height != 74 &&
                            targetBlob.BoundingBox.Width != 246 && targetBlob.BoundingBox.Height != 51 &&
                            targetBlob.BoundingBox.Width != 155 && targetBlob.BoundingBox.Height != 12 &&
                            targetBlob.BoundingBox.Width != 247 && targetBlob.BoundingBox.Height != 31 &&
                            targetBlob.BoundingBox.Width != 182 && targetBlob.BoundingBox.Height != 150 &&
                            targetBlob.BoundingBox.Width != 276 && targetBlob.BoundingBox.Height != 184 &&
                            targetBlob.BoundingBox.Width != 276 && targetBlob.BoundingBox.Height != 184 &&
                            targetBlob.BoundingBox.Width != 238 && targetBlob.BoundingBox.Height != 53 &&
                            targetBlob.BoundingBox.Width != 183 && targetBlob.BoundingBox.Height != 106 &&
                            targetBlob.BoundingBox.Width != 250 && targetBlob.BoundingBox.Height != 122 &&
                            targetBlob.BoundingBox.Width != 252 && targetBlob.BoundingBox.Height != 149 &&
                            targetBlob.BoundingBox.Width != 228 && targetBlob.BoundingBox.Height != 334 &&
                            targetBlob.BoundingBox.Width != 179 && targetBlob.BoundingBox.Height != 88 &&
                            targetBlob.BoundingBox.Width != 241 && targetBlob.BoundingBox.Height != 242 &&
                            targetBlob.BoundingBox.Width != 237 && targetBlob.BoundingBox.Height != 81 &&
                            targetBlob.BoundingBox.Width != 237 && targetBlob.BoundingBox.Height != 81 &&
                            targetBlob.BoundingBox.Width != 201 && targetBlob.BoundingBox.Height != 235 &&
                            targetBlob.BoundingBox.Width != 212 && targetBlob.BoundingBox.Height != 237 &&
                            targetBlob.BoundingBox.Width != 212 && targetBlob.BoundingBox.Height != 255 &&
                            targetBlob.BoundingBox.Width != 187 && targetBlob.BoundingBox.Height != 195 &&
                            targetBlob.BoundingBox.Width != 185 && targetBlob.BoundingBox.Height != 223 &&
                            targetBlob.BoundingBox.Width != 297 && targetBlob.BoundingBox.Height != 228 &&
                            targetBlob.BoundingBox.Width != 202 && targetBlob.BoundingBox.Height != 231 &&
                            targetBlob.BoundingBox.Width != 235 && targetBlob.BoundingBox.Height != 248 &&
                            targetBlob.BoundingBox.Width != 241 && targetBlob.BoundingBox.Height != 129 &&
                            targetBlob.BoundingBox.Width != 220 && targetBlob.BoundingBox.Height != 286 &&
                            targetBlob.BoundingBox.Width != 291 && targetBlob.BoundingBox.Height != 160 &&
                            targetBlob.BoundingBox.Width != 204 && targetBlob.BoundingBox.Height != 238 &&
                            targetBlob.BoundingBox.Width != 205 && targetBlob.BoundingBox.Height != 233 &&
                            targetBlob.BoundingBox.Width != 206 && targetBlob.BoundingBox.Height != 215 &&
                            targetBlob.BoundingBox.Width != 254 && targetBlob.BoundingBox.Height != 249 &&
                            targetBlob.BoundingBox.Width != 260 && targetBlob.BoundingBox.Height != 240 &&
                            targetBlob.BoundingBox.Width != 254 && targetBlob.BoundingBox.Height != 113 &&
                            targetBlob.BoundingBox.Width != 181 && targetBlob.BoundingBox.Height != 234 &&
                            targetBlob.BoundingBox.Width != 174 && targetBlob.BoundingBox.Height != 183 &&
                            targetBlob.BoundingBox.Width != 190 && targetBlob.BoundingBox.Height != 76 &&
                            targetBlob.BoundingBox.Width != 273 && targetBlob.BoundingBox.Height != 203 &&
                            targetBlob.BoundingBox.Width != 236 && targetBlob.BoundingBox.Height != 191 &&
                            targetBlob.BoundingBox.Width != 236 && targetBlob.BoundingBox.Height != 191 &&
                            targetBlob.BoundingBox.Width != 203 && targetBlob.BoundingBox.Height != 256 &&
                            targetBlob.BoundingBox.Width != 184 && targetBlob.BoundingBox.Height != 99 &&
                            targetBlob.BoundingBox.Width != 222 && targetBlob.BoundingBox.Height != 258 &&
                            targetBlob.BoundingBox.Width != 257 && targetBlob.BoundingBox.Height != 262 &&
                            targetBlob.BoundingBox.Width != 171 && targetBlob.BoundingBox.Height != 105 &&
                            targetBlob.BoundingBox.Width != 215 && targetBlob.BoundingBox.Height != 207 &&
                            targetBlob.BoundingBox.Width != 271 && targetBlob.BoundingBox.Height != 208 &&
                            targetBlob.BoundingBox.Width != 271 && targetBlob.BoundingBox.Height != 204 &&
                            targetBlob.BoundingBox.Width != 271 && targetBlob.BoundingBox.Height != 211 &&
                            targetBlob.BoundingBox.Width != 194 && targetBlob.BoundingBox.Height != 273 &&
                            targetBlob.BoundingBox.Width != 271 && targetBlob.BoundingBox.Height != 220 &&
                            targetBlob.BoundingBox.Width != 214 && targetBlob.BoundingBox.Height != 190 &&
                            targetBlob.BoundingBox.Width != 214 && targetBlob.BoundingBox.Height != 162 &&
                            targetBlob.BoundingBox.Width != 214 && targetBlob.BoundingBox.Height != 168 &&
                            targetBlob.BoundingBox.Width != 280 && targetBlob.BoundingBox.Height != 199 &&
                            targetBlob.BoundingBox.Width != 169 && targetBlob.BoundingBox.Height != 33 &&
                            targetBlob.BoundingBox.Width != 277 && targetBlob.BoundingBox.Height != 259 &&
                            targetBlob.BoundingBox.Width != 288 && targetBlob.BoundingBox.Height != 192 &&
                            targetBlob.BoundingBox.Width != 213 && targetBlob.BoundingBox.Height != 91 &&
                            targetBlob.BoundingBox.Width != 218 && targetBlob.BoundingBox.Height != 192 &&
                            targetBlob.BoundingBox.Width != 168 && targetBlob.BoundingBox.Height != 89 &&
                            targetBlob.BoundingBox.Width != 219 && targetBlob.BoundingBox.Height != 305 &&
                            targetBlob.BoundingBox.Width != 277 && targetBlob.BoundingBox.Height != 259 &&
                            targetBlob.BoundingBox.Width != 288 && targetBlob.BoundingBox.Height != 192 &&
                            targetBlob.BoundingBox.Width != 218 && targetBlob.BoundingBox.Height != 192 &&
                            targetBlob.BoundingBox.Width != 251 && targetBlob.BoundingBox.Height != 79 &&
                            targetBlob.BoundingBox.Width != 197 && targetBlob.BoundingBox.Height != 253               
                            )
                        */    
                            //&& targetBlob.BoundingBox.Width < 240)
                        if (targetBlob.BoundingBox.Width > 70 && targetBlob.BoundingBox.Height > 80)
                        /* 
                         
                         
                        if (((targetBlob.BoundingBox.Width > 150 && targetBlob.BoundingBox.Width < 240 && targetBlob.BoundingBox.Height >= 140 
                              && targetBlob.BoundingBox.Height < 200) || (targetBlob.BoundingBox.Width > 100 && targetBlob.BoundingBox.Width <= 160 
                            && targetBlob.BoundingBox.Height > 120 && targetBlob.BoundingBox.Height < 150) C (targetBlob.BoundingBox.Width > 65 
                            && targetBlob.BoundingBox.Width < 100 && targetBlob.BoundingBox.Height > 83 && targetBlob.BoundingBox.Height < 105)) 
                            && targetBlob.Area > 250 && targetBlob.Area < 720 && targetBlob.BoundingBox.Bottom > 260 && targetBlob.BoundingBox.Bottom < 300)
                        //&& targetBlob.BoundingBox.Right > 330 && targetBlob.BoundingBox.Right < 500 
                        */
                        {
                            //this.matchesw_ = targetBlob.BoundingBox.Width;
                            this.matches3 = targetBlob.Centroid;
                           
                            //this.matches4 = targetBlob.BoundingBox.Size;
                            this.matches5 = targetBlob.BoundingBox;
                            this._coincid = 1;
                            //this.blob = canny;
                            //this.matchesh = targetBlob.BoundingBox.Height;
                            //Image<Gray, byte> img2 = blobImg.Convert<Gray, byte>();

                           // Rectangle box = contours.BoundingRectangle;
                            using (Image<Gray, Byte> tmp = bin01_.Copy(matches5))
                            candidate = tmp.Convert<Gray, byte>();
                            BoxList.Add(candidate);
                            
                        }
                            
                    }
                    
                     
                    //match2 = this.matches3;
                    //match5 = this.matches4; 
                    match6 = this.matches5;
                    boxList.Add(match6);
                    match2 = this.matches3;
                    blobimg = blobImg;
                    Smoothed = smoothedRedMask;
                    Cannyb = cannyb;

                    coincid = _coincid;
                    //this.blob;
                   // matchw = this.matchesw_;
                    //match_h = this.matchesh;
                    
                }
                
            }
        }
    }
}