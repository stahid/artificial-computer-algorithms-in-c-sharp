using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.Util;
using Emgu.CV.VideoSurveillance;
using Emgu.CV.UI;
using Emgu.CV.CvEnum;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Threading;


namespace Methods_Detector
{
    public class Blob_Tracker
    {
        private static MCvFont _font = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_SIMPLEX, 1.0, 1.0);
        // private static Capture _cameraCapture;
        private static BlobTrackerAuto<Bgr> _tracker;
        private static IBGFGDetector<Bgr> _detector;
        //public Image<Bgr, Byte> prueba; //= _cameraCapture.QueryFrame();
        //public Image<Gray, Byte> forgroundMask;
        public string z1;

        public Blob_Tracker()
        {
            //InitializeComponent();
            //_detector = new FGDetector<Bgr>(FORGROUND_DETECTOR_TYPE.FGD);

            //_tracker = new BlobTrackerAuto<Bgr>();
            //Run();
            //System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
            /*
            t.Interval = 1500; // specify interval time as you want
            //t.Tick += new EventHandler(timer_Tick);
            t.Enabled = true;
            t.Start();
             * */
        }

        /*
      private void InitializeComponent()
      {
          throw new NotImplementedException();
      }
        */

        void Run()
        {/*
         try
         {
            _cameraCapture = new Capture();
         }
         catch (Exception e)
         {
            MessageBox.Show(e.Message);
            return;
         }
         */
            // _detector = new FGDetector<Bgr>(FORGROUND_DETECTOR_TYPE.FGD);

            // _tracker  = new BlobTrackerAuto<Bgr>();
            //Application.Idle += ProcessFrame;

        }


        public void ProcessFrame(Image<Bgr, byte> frame,Image<Gray, Byte> forgroundMask, out Image<Bgr, byte> img, out Image<Gray, byte> forgroundMask_,out int c, out string z)
        {
            _detector = new FGDetector<Bgr>(FORGROUND_DETECTOR_TYPE.FGD);
            _tracker = new BlobTrackerAuto<Bgr>();
            
            using (new MemStorage())
            {
                
                frame._SmoothGaussian(3); //filter out noises

                #region use the BG/FG detector to find the forground mask
                _detector.Update(frame);
                forgroundMask = _detector.ForegroundMask;
                #endregion

                _tracker.Process(frame, forgroundMask);

                foreach (MCvBlob blob in _tracker)
                {
                    if (blob.Size.Height > 40.00)
                    {
                        frame.Draw((Rectangle)blob, new Bgr(0, 0, 255.0), 2);
                        frame.Draw(blob.ID.ToString(), ref _font, Point.Round(blob.Center), new Bgr(255.0, 255.0, 255.0));
                         this.z1 = blob.Size.ToString();
                        Console.WriteLine("Size:" + z1 + "" + "ID:" + blob.ID.ToString() + "" + "Center:" + blob.Center.ToString());
                        
                    }
                    /*
                    frame.Draw((Rectangle)blob, new Bgr(0, 0, 255.0), 2);
                    frame.Draw(blob.ID.ToString(), ref _font, Point.Round(blob.Center), new Bgr(255.0, 255.0, 255.0));
                    string z = blob.Size.ToString();
                    Console.WriteLine("Size:" + z + "" + "ID:" + blob.ID.ToString() + "" + "Center:" + blob.Center.ToString());
                    //WriteLine(blob.ID.ToString());//z);
                    // Console.WriteLine("ID", blob.ID.ToString());
                    // Console.WriteLine("Size", blob.Size.ToString());
                    */
                }
                //Imagen(out frame, out forgroundMask);
                //img = frame;//.Resize(imageBox1.Width, imageBox1.Height, INTER.CV_INTER_LINEAR, false);
                //forgroundMask_ = forgroundMask;//.Resize(imageBox2.Width, imageBox2.Height, INTER.CV_INTER_LINEAR, false);

                //imageBox1.Image = frameDisplay;
                //imageBox2.Image = fgMaskDisplay;
                //imageBox1.Image = frame;
                //imageBox2.Image = forgroundMask;
            }

            img = frame;//.Resize(imageBox1.Width, imageBox1.Height, INTER.CV_INTER_LINEAR, false);
            forgroundMask_ = forgroundMask;//.Resize(imageBox2.Width, imageBox2.Height, INTER.CV_INTER_LINEAR, false);
            c = 2;
            z = z1;
            
            /*
          public void Imagen(out Image<Bgr, byte> img, out Image<Gray, byte> forgroundMask_)
          {
              img = frame;//.Resize(imageBox1.Width, imageBox1.Height, INTER.CV_INTER_LINEAR, false);
              forgroundMask_ = forgroundMask;//.Resize(imageBox2.Width, imageBox2.Height, INTER.CV_INTER_LINEAR, false);
          }
            */
        }
    }
} 